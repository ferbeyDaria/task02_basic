package com.epam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Represents an Main.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-07
 */
public final class Main {
    /**
     * Private default constructor.
     */
    private Main() {

    }

    /**
     * This is the  main method which makes use getOddNumber, getEvenNumber
     * and fibs methods.
     * Also this is the main method print the max even and max odd Fibonacci number,
     * search for and print percentage of odd Fibonacci numbers and percentage
     * of even Fibonacci numbers
     *
     * @param args unused
     */

    public static void main(final String[] args) {
        OddNumber oddNum = new OddNumber();
        EvenNumber evenNum = new EvenNumber();
        FibonacciNumber fibNum = new FibonacciNumber();

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter start: ");
        int start = scan.nextInt();
        System.out.print("Enter end: ");
        int end = scan.nextInt();
        System.out.println("[" + start + ";" + end + "]");

        //print list of odd numbers
        ArrayList<Integer> oddList = oddNum.getOddNum(start, end);
        System.out.println("List of odd numbers: " + oddList);

        //print sum of odd numbers
        int sumOdd = 0;
        for (Integer i : oddList) {
            sumOdd += i;
        }
        System.out.println("Sum of odd number: " + sumOdd);

        //print list of even numbers
        ArrayList<Integer> evenList = evenNum.getEvenNm(start, end);
        System.out.println("List of even numbers: " + evenList);

        //print sum of even numbers
        int sumEven = 0;
        for (Integer i : evenList) {
            sumEven += i;
        }
        System.out.println("Sum of even number: " + sumEven);

        //print numbers of Fibonacci
        ArrayList<Integer> fib = fibNum.fibs(end);
        System.out.println("Fibonacci: " + fib);

        //search for max even Fibonacci number and max odd Fibonacci number
        ArrayList<Integer> fibOdd = new ArrayList<Integer>();
        ArrayList<Integer> fibEven = new ArrayList<Integer>();
        int maxOdd = 0;
        int maxEven = 0;
        for (Integer i : fib) {
            if (i % 2 == 1) {
                fibOdd.add(i);
            } else {
                fibEven.add(i);
            }
        }
        maxOdd = Collections.max(fibOdd);
        maxEven = Collections.max(fibEven);
        System.out.println("Max Even: " + maxEven + " " + "Max Odd: " + maxOdd);

        //print percentage of odd Fibonacci numbers and percentage of even
        // Fibonaci numbers
        final int percentage = 100;
        double percOddNum = (fibOdd.size() * percentage) / fib.size();
        double percEvenNum = (fibEven.size() * percentage) / fib.size();
        System.out.println("The percent of even numbers is: " + percEvenNum);
        System.out.println("The percent of odd numbers is: " + percOddNum);

    }


}
