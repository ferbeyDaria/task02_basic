package com.epam;

import java.util.ArrayList;

/**
 * Represents an OddNumber.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-07
 */
public final class OddNumber {
    /**
     * Private default constructor.
     */
    OddNumber() {

    }

    /**
     * Gets the list of odd number code.
     *
     * @param start the start code
     * @param end   the end code
     * @return the list of odd number
     */
    public static ArrayList<Integer> getOddNum(final int start, final int end) {
        ArrayList<Integer> num = new ArrayList<Integer>();
        if (start < end) {
            for (int i = start; i < end; i++) {
                if (i % 2 == 1) {
                    num.add(i);
                    i++;
                } else if (i % 2 == 0) {
                    i++;
                    num.add(i);
                }
            }
        } else if (start > end) {
            for (int i = start; i > end; i--) {
                if (i % 2 == 1) {
                    num.add(i);
                } else if (i % 2 == 0) {
                    i--;
                    num.add(i);
                }
            }
        }
        return num;
    }

}
