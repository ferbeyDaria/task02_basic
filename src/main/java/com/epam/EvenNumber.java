package com.epam;

import java.util.ArrayList;

/**
 * Represents an EvenNumber.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-07
 */
public final class EvenNumber {
    /**
     * Private default constructor.
     */
    EvenNumber() {

    }

    /**
     * Gets the list of  even number code.
     *
     * @param start the start code
     * @param end   the end code
     * @return the list of even number
     */
    public static ArrayList<Integer> getEvenNm(final int start, final int end) {
        ArrayList<Integer> num = new ArrayList<Integer>();
        if (start < end) {
            for (int i = start; i < end; i++) {
                if (i % 2 == 0) {
                    num.add(i);
                    i++;
                } else if (i % 2 == 1) {
                    i++;
                    num.add(i);
                }
            }
        } else if (start > end) {
            for (int i = start; i > end; i--) {
                if (i % 2 == 0) {
                    num.add(i);
                } else if (i % 2 == 1) {
                    i--;
                    num.add(i);
                }
            }
        }
        return num;
    }
}

