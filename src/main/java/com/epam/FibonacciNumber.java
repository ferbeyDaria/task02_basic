package com.epam;

import java.util.ArrayList;

/**
 * Represents an FibonacciNumber.
 *
 * @author Ferbey Daria
 * @version 1.6
 * @since 2019-11-07
 */
public final class FibonacciNumber {
    /**
     * Private default constructor.
     */
    FibonacciNumber() {

    }

    /**
     * Gets the numbers of Fibonacci.
     *
     * @param amountOfNum the number of numbers in a number of Fibonacci code
     * @return the list of number of Fibonscci code
     */
    public static ArrayList<Integer> fibs(final int amountOfNum) {
        ArrayList<Integer> fib = new ArrayList<Integer>();
        int n0 = 1;
        int n1 = 1;
        fib.add(n0);
        fib.add(n1);
        int n2;
        final int iter = 3;
        for (int i = iter; i <= amountOfNum; i++) {
            n2 = n0 + n1;
            n0 = n1;
            n1 = n2;
            fib.add(n2);
        }
        return fib;
    }
}
